import XCTest

#if !os(macOS)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(drhttp_swiftTests.allTests),
    ]
}
#endif