import XCTest
@testable import drhttp

final class drhttp_Tests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(drhttp().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
