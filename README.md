This is the official [swift](https://swift.org/) client for [DrHTTP](https://drhttp.com/) service.

DrHTTP lets you record two types of requests :

 - Inbound: requests that are performed by clients of your server (e.g. API calls from a mobile app)
 - Outbound (optional): requests that are performed by your server (e.g. API calls to third parties)

*Note: You need to configure inbound request recording to have outbound request recording working.*

# Installation for [Vapor](https://vapor.codes/)

At the moment, DrHTTP only supports [Vapor web framework](https://vapor.codes/).
An integration example is provided [here](https://bitbucket.org/drhttp/drhttp-swift/src/master/Examples/Vapor/)

## Package

Add the following dependency in your project's `package.swift`:

```swift
let package = Package(
	...
	dependencies: [
    	...
		.package(url: "https://bitbucket.org/drhttp/drhttp-swift.git", from: "1.1.0"),
		...
	]
	...
)
```

## Import

To use the `DrHTTP` features, simply add the following import statement to your source file:

```swift
import DrHTTP
```

## Request recording


### Inbound request recording

In order to start recording inbound requests, need to generate an API key (`drhttp_api_key`). Genrate a DrHTTP API key [here](https://drhttp.com/).

Recording is done via a Vapor "Middleware". You can configure it in your project's `configure.swift`:

```swift
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
	DrHTTP.setup(dsn: "https://<drhttp_api_key>@api.drhttp.com")
	...
	var middlewares = MiddlewareConfig()
	middlewares.use(DrHTTPMiddleware(identify: { request in
	    let userId = request.http.headers.firstValue(name: .init("user"))
	    let deviceId = request.http.headers.firstValue(name: .init("device"))
	    return (userId, deviceId)
	}))
	middlewares.use(ErrorMiddleware.self)
	services.register(middlewares)
	...
}
```

The `identify` closure in `DrHTTPMiddleware`'s init is optional. It allows the identification of the inbound request. You can specify a `userID` and/or a `deviceId`. You'll be able to filter requests based on those fields in the app's interface.


### Outbound request recording

In order to start recording outbound request, you need to configure recording for inbound requests.

Recording is done via a Vapor "Client". You can configure it in your project's `configure.swift`:

```swift
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
	...
	services.register(DrHTTPClient.self)
	config.prefer(DrHTTPClient.self, for: Client.self)
	...
}
```

Then you need to activate recording on any outbound request you want to record by calling `activateTracing(linkedTo:)` on the client :

```swift
router.get { (request: Request) -> String in
    let externalUrl = "http://vapor.codes"
    let client = try request.client().activateTracing(linkedTo: request)
    let _ = client.get(externalUrl).map({ response in
        
    })
    return "Hello, world!"
}
```

This will enable the link between the inbound request (`request`) and the outbound request made by the client (`client`).

#### Outbound request recording when already using a custom client

If you already have a custom client that you are using in your app. Let's call it `MyClient`. You can configure `DrHTTPClient` to use it :

```swift
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
	...
	DrHTTPClient.interceptedClientClass = MyClient.self
	services.register(DrHTTPClient.self)
	config.prefer(DrHTTPClient.self, for: Client.self)
	...
}
```

`DrHTTPClient` will record the request, pass it to your client, record the response from your client, return the response.

# Troubleshooting

Please [report any issue](https://bitbucket.org/drhttp/drhttp-swift/issues/new) you encounter regarding the documentation or implementation. This is very much appreciated.