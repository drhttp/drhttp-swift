import Vapor

fileprivate let TracingIDHeaderName = HTTPHeaderName("X-DrHTTP-TracingID")

extension Request {
    var tracingID : String? {
        get {
            return self.http.headers.firstValue(name: TracingIDHeaderName)
        }
        set {
            if let newValue = newValue {
                self.http.headers.replaceOrAdd(name: TracingIDHeaderName, value: newValue)
            } else {
                self.http.headers.remove(name: TracingIDHeaderName)
            }
        }
    }
}
