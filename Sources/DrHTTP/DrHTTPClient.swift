import Vapor

public final class DrHTTPClient : ServiceType, Client {
    static var interceptedClientClass = FoundationClient.self
    
    // MARK: ServiceType
    
    public static var serviceSupports: [Any.Type] {
        return [Client.self]
    }
    
    public static func makeService(for worker: Container) throws -> DrHTTPClient {
        return DrHTTPClient(container: worker)
    }
    
    // MARK: Client
    
    public let container: Container
    fileprivate var parentRequest: Request?
    
    private init(container: Container) {
        self.container = container
    }
    
    public func send(_ request: Request) -> EventLoopFuture<Response> {
        let logger = try? self.container.make(Logger.self)
        
        let traceId = self.parentRequest?.tracingID // Fetch it now so we are almost sure it won't be modified async
        if let traceId = traceId {
            logger?.debug("Client : initiated by request : \(traceId)")
        }
        
        let client = try! request.make(DrHTTPClient.interceptedClientClass)
        let date = Date()
        return client.send(request).map({ response in
            if let traceId = traceId {
                try DrHTTP.record(datetime: date, parentRequestTraceID: traceId,
                                      request: request, response: response, worker: self.container)
            } else {
                logger?.warning("Client : as parent request is not specified, we did not record call to \(request.http.url). See Client.activateTracing(linkedTo)")
            }
            logger?.debug("Client : received [\(response.http.status)] \(request.http.url)")
            return response
        })
    }
}

extension Client {
    // Pass the request that came into the server as parentRequest
    // This will enable the tracing of the
    public func activateTracing(linkedTo parentRequest: Request) -> Self {
        if let drhttpclient = self as? DrHTTPClient {
            drhttpclient.parentRequest = parentRequest
        }
        return self
    }
}
