import Vapor

public class DrHTTPMiddleware : Middleware {
    private var identify : ((Request) -> (userId: String?, deviceId: String?))?

    public init() {}    // Fixes a visibility error when initializing an object of a package
         
    public convenience init(identify: ((Request) -> (userId: String?, deviceId: String?))?) {
        self.init()
        self.identify = identify
    }

    public func respond(to request: Request, chainingTo next: Responder) throws -> Future<Response> {
        let logger = try? request.make(Logger.self)
        logger?.debug("Middleware : request requested : \(request.http.url)")
        let date = Date()
        
        // Set tracing ID on request
        if let _ = request.tracingID {
            throw ServiceError(identifier: "DrHTTP middleware respond",
                               reason: "Middelware request tracing header already set",
                               suggestedFixes: ["Make sure DrHTTPMiddleware is not registered twice as a middleware"])
        } else {
            request.tracingID = UUID().uuidString
            logger?.debug("Middleware : set tracingID : \(request.tracingID!)")
        }
                 

        // Retrieve user & device identifiers
        let identifiers = self.identify?(request)

        // Intercept response
        let response = try next.respond(to: request)
        return response.map { response in
            try DrHTTP.record(datetime: date, userIdentifier: identifiers?.userId, deviceIdentifier: identifiers?.deviceId,
                              request: request, response: response, worker: request)
            logger?.debug("Middleware : request responded : \(response.http.body)")
            return response
        }
    }
}

