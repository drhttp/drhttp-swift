import Vapor

public final class DrHTTP {
    struct Configuration {
        let scheme: HTTPScheme
        let apiKey: String
        let hostname: String
        
        init(dsn: String) {
            let components = dsn.components(separatedBy: CharacterSet(charactersIn: ":/@")).filter({ $0.count > 0 })
            self.scheme = components[0] == "https" ? .https : .http
            self.apiKey = components[1]
            self.hostname = components[2]
        }
    }
    
    static private(set) var configuration : Configuration?
    
    public static func setup(dsn: String) {
        self.configuration = .init(dsn: dsn)
    }
    
    static func record(datetime: Date = Date(), userIdentifier: String? = nil, deviceIdentifier: String? = nil, parentRequestTraceID: String? = nil,
                       request: Request, response: Response? = nil, worker: Worker) throws {
        var data : [String:Any] = [
            "datetime" : DateFormatter.iso8601.string(from: datetime),
            "method" : request.http.method.string,
            "uri" : request.http.url.absoluteString,
            "status" : String(response?.http.status.code ?? 500),
            "request" : self.parse(httpMessage: request.http)
        ]
        data["id"] = request.tracingID?.lowercased()
        data["parent_id"] = parentRequestTraceID?.lowercased()
        data["user_identifier"] = userIdentifier
        data["device_identifier"] = deviceIdentifier
        if let response = response {
            data["response"] = self.parse(httpMessage: response.http)
        }
        
        try self.send(data: data, worker: worker)
    }
    
    private static func parse(httpMessage: HTTPMessage) -> [String:Any] {
        var resp : [String:Any] = [
            "headers": Dictionary(uniqueKeysWithValues: httpMessage.headers.map({ ($0.name, $0.value) }))
        ]
        if let data = httpMessage.body.data, let body = String(bytes: data, encoding: .utf8) {
            resp["body"] = body
        }
        return resp
    }
    
    private static func send(data: [String:Any], worker: Worker) throws {
        guard let configuration = DrHTTP.configuration else {
            throw ServiceError(identifier: "DrHTTP send",
                               reason: "Service is not configured",
                               suggestedFixes: ["Call DrHTTP.setup(dsn: \"...\") in configure.swift"])
        }
        
        let _ = HTTPClient.connect(scheme: configuration.scheme , hostname: configuration.hostname, on: worker).map({ client in
            var request = HTTPRequest(method: .POST, url: "/api/v1/http_record/record")
            request.headers.add(name: .contentType, value: "application/json; charset=utf-8")
            request.headers.add(name: HTTPHeaderName("DrHTTP-ApiKey"), value: configuration.apiKey)
            let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            request.body = HTTPBody(data: jsonData)
            let _ = client.send(request).map({ response in
            })
        })
    }
}

fileprivate extension DateFormatter {
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        return formatter
    }()
}
