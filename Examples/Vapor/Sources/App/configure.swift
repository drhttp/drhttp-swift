import Vapor
import DrHTTP

public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
    DrHTTP.setup(dsn: "https://<api_key>@api.drhttp.com")
    
    /// Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)
    
    /// Register middleware
    var middlewares = MiddlewareConfig()
    middlewares.use(DrHTTPMiddleware(identify: { request in
        let userId = request.http.headers.firstValue(name: .init("user"))
        let deviceId = request.http.headers.firstValue(name: .init("device"))
        return (userId, deviceId)
    }))
    middlewares.use(ErrorMiddleware.self)
    services.register(middlewares)
    
    //DrHTTPClient.interceptedClientClass = ...
    services.register(DrHTTPClient.self)
    config.prefer(DrHTTPClient.self, for: Client.self)
}
