import Vapor
import DrHTTP

public func routes(_ router: Router) throws {
    router.get { (request: Request) -> String in
        let externalUrl = "http://vapor.codes"
        let client = try request.client().activateTracing(linkedTo: request)
        let _ = client.get(externalUrl).map({ response in
            
        })
        return "Hello, world!"
    }
    
    router.post { (req: Request) -> String in
        if let body = req.http.body.data, let bodyString = String(data: body, encoding: .utf8) {
            return "this is your data : \(bodyString)"
        } else {
            return "Can't read data"
        }
    }
}
