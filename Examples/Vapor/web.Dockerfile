# We build swift code at runtime on purpose to use the cache provided
# by mounted build dir (not possible during image creation, only at container runtime)

FROM swift:4.2

RUN apt-get -qq update && apt-get -q -y install \
  libicu55 libxml2 libbsd0 libcurl3 libatomic1 \
  tzdata \
  && rm -r /var/lib/apt/lists/*

WORKDIR /app
COPY . .

ENTRYPOINT swift build -c release \
  && `swift build -c release --show-bin-path`/Run serve --hostname 0.0.0.0 --port 80
