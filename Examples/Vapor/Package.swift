// swift-tools-version:4.0
import PackageDescription

let package = Package(
    name: "drhttp-example-swift",
    dependencies: [
        .package(url: "https://github.com/vapor/vapor.git", from: "3.0.0"),
        .package(url: "https://bitbucket.org/drhttp/drhttp-swift.git", from: "1.1.0"),
    ],
    targets: [
        .target(name: "App", dependencies: ["Vapor", "DrHTTP"]),
        .target(name: "Run", dependencies: ["App"]),
        .testTarget(name: "AppTests", dependencies: ["App"])
    ]
)

