// swift-tools-version:4.2

import PackageDescription

let package = Package(
    name: "drhttp",
    products: [
        .library(name: "DrHTTP", targets: ["DrHTTP"]),
    ],
    dependencies: [
        .package(url: "https://github.com/vapor/vapor.git", from: "3.0.0"),
    ],
    targets: [
        .target(name: "DrHTTP", dependencies: ["Vapor"]),
        .testTarget(name: "drhttpTests", dependencies: ["DrHTTP"]),
    ]
)
